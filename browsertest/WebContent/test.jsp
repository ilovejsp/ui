<%@page import="java.util.*,java.net.*,java.text.*,java.util.zip.*,java.io.*"%>
<%@page import="com.lotte.ts.filedirectory.*" %>
<%@page import="com.lotte.ts.util.*" %>
<%
		//Get the current browsing directory
		request.setAttribute("dir", request.getParameter("dir"));
		// The browser_name variable is used to keep track of the URI
		// of the jsp file itself.  It is used in all link-backs.
		final String browser_name = request.getRequestURI();
		final String FOL_IMG = "";
		boolean nohtml = false;
		boolean dir_view = true;
		// View file
		if (request.getParameter("file") != null) {
            File f = new File(request.getParameter("file"));
            if (!Utils.isAllowed(f)) {
                request.setAttribute("dir", f.getParent());
                request.setAttribute("error", "You are not allowed to access "+f.getAbsolutePath());
            }
            else if (f.exists() && f.canRead()) {
                if (Utils.isPacked(f.getName(), false)) {
                    //If zipFile, do nothing here
                }
                else{
                    String mimeType = Utils.getMimeType(f.getName());
                    response.setContentType(mimeType);
                    if (mimeType.equals("text/plain")) response.setHeader(
                            "Content-Disposition", "inline;filename=\"temp.txt\"");
                    else response.setHeader("Content-Disposition", "inline;filename=\""
                            + f.getName() + "\"");
                    BufferedInputStream fileInput = new BufferedInputStream(new FileInputStream(f));
                    byte buffer[] = new byte[8 * 1024];
                    out.clearBuffer();
                    OutputStream out_s = new Writer2Stream(out);
                    Utils.copyStreamsWithoutClose(fileInput, out_s, buffer);
                    fileInput.close();
                    out_s.flush();
                    nohtml = true;
                    dir_view = false;
                }
            }
            else {
                request.setAttribute("dir", f.getParent());
                request.setAttribute("error", "File " + f.getAbsolutePath()
                        + " does not exist or is not readable on the server");
            }
		}

		// Download file
		else if (request.getParameter("downfile") != null) {
			String filePath = request.getParameter("downfile");
			File f = new File(filePath);
			if (!Utils.isAllowed(f)){
				request.setAttribute("dir", f.getParent());
				request.setAttribute("error", "You are not allowed to access " + f.getAbsoluteFile());
			}
			else if (f.exists() && f.canRead()) {
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment;filename=\"" + f.getName()
						+ "\"");
				response.setContentLength((int) f.length());
				BufferedInputStream fileInput = new BufferedInputStream(new FileInputStream(f));
				byte buffer[] = new byte[8 * 1024];
				out.clearBuffer();
				OutputStream out_s = new Writer2Stream(out);
				Utils.copyStreamsWithoutClose(fileInput, out_s, buffer);
				fileInput.close();
				out_s.flush();
				nohtml = true;
				dir_view = false;
			}
			else {
				request.setAttribute("dir", f.getParent());
				request.setAttribute("error", "File " + f.getAbsolutePath()
						+ " does not exist or is not readable on the server");
			}
		}
		if (nohtml) return;
		//else
			// If no parameter is submitted, it will take the path from jsp file browser
			if (request.getAttribute("dir") == null) {
				String path = null;
				if (application.getRealPath(request.getRequestURI()) != null) path = new File(
						application.getRealPath(request.getRequestURI())).getParent();
				if (path == null) { // handle the case where we are not in a directory (ex: war file)
					path = new File(".").getAbsolutePath();
				}
				//Check path
                if (!Utils.isAllowed(new File(path))){
                    if (Utils.RESTRICT_PATH.indexOf(";")<0) path = Utils.RESTRICT_PATH;
                    else path = Utils.RESTRICT_PATH.substring(0, Utils.RESTRICT_PATH.indexOf(";"));
                }
				request.setAttribute("dir", path);
			}%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<meta name="robots" content="noindex">
<meta http-equiv="expires" content="0">
<meta http-equiv="pragma" content="no-cache">
	<style type="text/css">
		.button {background-color: #c0c0c0; color: #666666;
		border: 1px solid #999999; }
		.button:Hover { color: #444444 }
		table.filelist {background-color:#666666; width:100%; border:0px none #ffffff}
		th { background-color:#c0c0c0 }
		tr.mouseout { background-color:#ffffff; }
		tr.mousein  { background-color:#eeeeee; }
		tr.checked  { background-color:#cccccc }
		tr.mousechecked { background-color:#c0c0c0 }
		td { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; color: #666666;}
		td.message { background-color: #FFFF00; color: #000000; text-align:center; font-weight:bold}
		td.error { background-color: #FF0000; color: #000000; text-align:center; font-weight:bold}
		A { text-decoration: none; }
		A:Hover { color : Red; text-decoration : underline; }
		BODY { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; color: #666666;}
	</style>
	<%
		
        //Check path
        if (!Utils.isAllowed(new File((String)request.getAttribute("dir")))){
            request.setAttribute("error", "You are not allowed to access " + request.getAttribute("dir"));
        }
		//Upload monitor
		else if (request.getParameter("uplMonitor") != null) {%>
	<style type="text/css">
		BODY { font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 8pt; color: #666666;}
	</style><%
			String fname = request.getParameter("uplMonitor");
			//First opening
			boolean first = false;
			if (request.getParameter("first") != null) first = true;
			UpInfo info = new UpInfo();
			if (!first) {
				info = UploadMonitor.getInfo(fname);
				if (info == null) {
					//Windows
					int posi = fname.lastIndexOf("\\");
					if (posi != -1) info = UploadMonitor.getInfo(fname.substring(posi + 1));
				}
				if (info == null) {
					//Unix
					int posi = fname.lastIndexOf("/");
					if (posi != -1) info = UploadMonitor.getInfo(fname.substring(posi + 1));
				}
			}
			dir_view = false;
			request.setAttribute("dir", null);
			if (info.aborted) {
				UploadMonitor.remove(fname);
				%>
</head>
<body>
<b>Upload of <%=fname%></b><br><br>
Upload aborted.</body>
</html><%
			}
			else if (info.totalSize != info.currSize || info.currSize == 0) {
				%>
<META HTTP-EQUIV="Refresh" CONTENT="<%=Utils.UPLOAD_MONITOR_REFRESH%>;URL=<%=browser_name %>?uplMonitor=<%=URLEncoder.encode(fname)%>">
</head>
<body>
<b>Upload of <%=fname%></b><br><br>
<center>
<table height="20px" width="90%" bgcolor="#eeeeee" style="border:1px solid #cccccc"><tr>
<td bgcolor="blue" width="<%=info.getPercent()%>%"></td><td width="<%=100-info.getPercent()%>%"></td>
</tr></table></center>
<%=Utils.convertFileSize(info.currSize)%> from <%=Utils.convertFileSize(info.totalSize)%>
(<%=info.getPercent()%> %) uploaded (Speed: <%=info.getUprate()%>).<br>
Time: <%=info.getTimeElapsed()%> from <%=info.getTimeEstimated()%>
</body>
</html><%
			}
			else {
				UploadMonitor.remove(fname);
				%>
</head>
<body onload="javascript:window.close()">
<b>Upload of <%=fname%></b><br><br>
Upload finished.
</body>
</html><%
			}
		}

		// Upload
		else if ((request.getContentType() != null)
				&& (request.getContentType().toLowerCase().startsWith("multipart"))) {
			response.setContentType("text/html");
			HttpMultiPartParser parser = new HttpMultiPartParser();
			boolean error = false;
			try {
				int bstart = request.getContentType().lastIndexOf("oundary=");
				String bound = request.getContentType().substring(bstart + 8);
				int clength = request.getContentLength();
				Hashtable ht = parser.processData(request.getInputStream(), bound, Utils.tempdir, clength);
                if (!Utils.isAllowed(new File((String)ht.get("dir")))){
                    request.setAttribute("error", "You are not allowed to access " + ht.get("dir"));
                    error = true;
                }
				else if (ht.get("myFile") != null) {
					FileInfo fi = (FileInfo) ht.get("myFile");
					File f = fi.file;
					UpInfo info = UploadMonitor.getInfo(fi.clientFileName);
					if (info != null && info.aborted) {
						f.delete();
						request.setAttribute("error", "Upload aborted");
					}
					else {
						// Move file from temp to the right dir
						String path = (String) ht.get("dir");
						if (!path.endsWith(File.separator)) path = path + File.separator;
						if (!f.renameTo(new File(path + f.getName()))) {
							request.setAttribute("error", "Cannot upload file.");
							error = true;
							f.delete();
						}
					}
				}
				else {
					request.setAttribute("error", "No file selected for upload");
					error = true;
				}
				request.setAttribute("dir", (String) ht.get("dir"));
			}
			catch (Exception e) {
				request.setAttribute("error", "Error " + e + ". Upload aborted");
				error = true;
			}
			if (!error) request.setAttribute("message", "File upload correctly finished.");
		}

		// Delete Files
		else if ((request.getParameter("Submit") != null)
				&& (request.getParameter("Submit").equals(Utils.DELETE_FILES))) {
			Vector v = Utils.expandFileList(request.getParameterValues("selfile"), true);
			boolean error = false;
			//delete backwards
			for (int i = v.size() - 1; i >= 0; i--) {
				File f = (File) v.get(i);
                if (!Utils.isAllowed(f)){
                    request.setAttribute("error", "You are not allowed to access " + f.getAbsolutePath());
                    error = true;
                    break;
                }
				if (!f.canWrite() || !f.delete()) {
					request.setAttribute("error", "Cannot delete " + f.getAbsolutePath()
							+ ". Deletion aborted");
					error = true;
					break;
				}
			}
			if ((!error) && (v.size() > 1)) request.setAttribute("message", "All files deleted");
			else if ((!error) && (v.size() > 0)) request.setAttribute("message", "File deleted");
			else if (!error) request.setAttribute("error", "No files selected");
		}
		// Create Directory
		else if ((request.getParameter("Submit") != null)
				&& (request.getParameter("Submit").equals(Utils.CREATE_DIR))) {
			String dir = "" + request.getAttribute("dir");
			String dir_name = request.getParameter("cr_dir");
			String new_dir = Utils.getDir(dir, dir_name);
            if (!Utils.isAllowed(new File(new_dir))){
                request.setAttribute("error", "You are not allowed to access " + new_dir);
            }
			else if (new File(new_dir).mkdirs()) {
				request.setAttribute("message", "Directory created");
			}
			else request.setAttribute("error", "Creation of directory " + new_dir + " failed");
		}
	
		// Rename a file
		else if ((request.getParameter("Submit") != null)
				&& (request.getParameter("Submit").equals(Utils.RENAME_FILE))) {
			Vector v = Utils.expandFileList(request.getParameterValues("selfile"), true);
			String dir = "" + request.getAttribute("dir");
			String new_file_name = request.getParameter("cr_dir");
			String new_file =Utils. getDir(dir, new_file_name);
            if (!Utils.isAllowed(new File(new_file))){
                request.setAttribute("error", "You are not allowed to access " + new_file);
            }
			// The error conditions:
			// 1) Zero Files selected
			else if (v.size() <= 0) request.setAttribute("error",
					"Select exactly one file or folder. Rename failed");
			// 2a) Multiple files selected and the first isn't a dir
			//     Here we assume that expandFileList builds v from top-bottom, starting with the dirs
			else if ((v.size() > 1) && !(((File) v.get(0)).isDirectory())) request.setAttribute(
					"error", "Select exactly one file or folder. Rename failed");
			// 2b) If there are multiple files from the same directory, rename fails
			else if ((v.size() > 1) && ((File) v.get(0)).isDirectory()
					&& !(((File) v.get(0)).getPath().equals(((File) v.get(1)).getParent()))) {
				request.setAttribute("error", "Select exactly one file or folder. Rename failed");
			}
			else {
				File f = (File) v.get(0);
                if (!Utils.isAllowed(f)){
                    request.setAttribute("error", "You are not allowed to access " + f.getAbsolutePath());
                }
				// Test, if file_name is empty
				else if ((new_file.trim() != "") && !new_file.endsWith(File.separator)) {
					if (!f.canWrite() || !f.renameTo(new File(new_file.trim()))) {
						request.setAttribute("error", "Creation of file " + new_file + " failed");
					}
					else request.setAttribute("message", "Renamed file "
							+ ((File) v.get(0)).getName() + " to " + new_file);
				}
				else request.setAttribute("error", "Error: \"" + new_file_name
						+ "\" is not a valid filename");
			}
		}
		// Directory viewer
		if (dir_view && request.getAttribute("dir") != null) {
			File f = new File("" + request.getAttribute("dir"));
			//Check, whether the dir exists
			if (!f.exists() || !Utils.isAllowed(f)) {
				if (!f.exists()){
                    request.setAttribute("error", "Directory " + f.getAbsolutePath() + " does not exist.");
                }
                else{
                    request.setAttribute("error", "You are not allowed to access " + f.getAbsolutePath());
                }
				//if attribute olddir exists, it will change to olddir
				if (request.getAttribute("olddir") != null && Utils.isAllowed(new File((String) request.getAttribute("olddir")))) {
					f = new File("" + request.getAttribute("olddir"));
				}
				//try to go to the parent dir
				else {
					if (f.getParent() != null && Utils.isAllowed(f)) f = new File(f.getParent());
				}
				//If this dir also do also not exist, go back to browser.jsp root path
				if (!f.exists()) {
					String path = null;
					if (application.getRealPath(request.getRequestURI()) != null) path = new File(
							application.getRealPath(request.getRequestURI())).getParent();
					if (path == null) // handle the case were we are not in a directory (ex: war file)
					path = new File(".").getAbsolutePath();
					f = new File(path);
				}
				if (Utils.isAllowed(f)) request.setAttribute("dir", f.getAbsolutePath());
                else request.setAttribute("dir", null);
			}
%>
<script type="text/javascript">
<!--
	<%// This section contains the Javascript used for interface elements %>
	var check = false;
	<%// Disables the checkbox feature %>
	function dis(){check = true;}
	var DOM = 0, MS = 0, OP = 0, b = 0;
	<%// Determine the browser type %>
	function CheckBrowser(){
		if (b == 0){
			if (window.opera) OP = 1;
			// Moz or Netscape
			if(document.getElementById) DOM = 1;
			// Micro$oft
			if(document.all && !OP) MS = 1;
			b = 1;
		}
	}
	<%// Allows the whole row to be selected %>
	function selrow (element, i){
		var erst;
		CheckBrowser();
		if ((OP==1)||(MS==1)) erst = element.firstChild.firstChild;
		else if (DOM==1) erst = element.firstChild.nextSibling.firstChild;
		<%// MouseIn %>
		if (i==0){
			if (erst.checked == true) element.className='mousechecked';
			else element.className='mousein';
		}
		<%// MouseOut %>
		else if (i==1){
			if (erst.checked == true) element.className='checked';
			else element.className='mouseout';
		}
		<%    // MouseClick %>
		else if ((i==2)&&(!check)){
			if (erst.checked==true) element.className='mousein';
			else element.className='mousechecked';
			erst.click();
		}
		else check=false;
	}

	function popUp(URL){
		fname = document.getElementsByName("myFile")[0].value;
		if (fname != "")
			window.open(URL+"?first&uplMonitor="+encodeURIComponent(fname),"","width=400,height=150,resizable=yes,depend=yes")
	}
//-->
</script>
<title><%=request.getAttribute("dir")%></title>
</head>
<body>
<%
			//Output message
			if (request.getAttribute("message") != null) {
				out.println("<table border=\"0\" width=\"100%\"><tr><td class=\"message\">");
				out.println(request.getAttribute("message"));
				out.println("</td></tr></table>");
			}
			//Output error
			if (request.getAttribute("error") != null) {
				out.println("<table border=\"0\" width=\"100%\"><tr><td class=\"error\">");
				out.println(request.getAttribute("error"));
				out.println("</td></tr></table>");
			}
            if (request.getAttribute("dir") != null){
%>
	<form action="<%= browser_name %>" method="Post" name="FileList">
	<table class="filelist" cellspacing="1px" cellpadding="0px">
<%
			// Output the table, starting with the headers.
			String dir = URLEncoder.encode("" + request.getAttribute("dir"));
			String cmd = browser_name + "?dir=" + dir;
			int sortMode = 1;
			if (request.getParameter("sort") != null) sortMode = Integer.parseInt(request
					.getParameter("sort"));
			int[] sort = new int[] {1, 2, 3, 4};

			char trenner = File.separatorChar;

			// Output all files and dirs and calculate the number of files and total size
			File[] entry =f.listFiles();
			if (entry == null) entry = new File[] {};
			long totalSize = 0; // The total size of the files in the current directory
			long fileCount = 0; // The count of files in the current working directory
			if (entry != null && entry.length > 0) {
				Arrays.sort(entry, new FileComp(sortMode));
				
				for (int i = 0; i < entry.length; i++) {
					if(!entry[i].getName().equals("WEB-INF") &&!entry[i].getName().equals("META-INF") &&!entry[i].getName().equals("test.jsp")){
							String name = URLEncoder.encode(entry[i].getAbsolutePath());
							String type = "File"; // This String will tell the extension of the file
							if (entry[i].isDirectory()) type = "DIR"; // It's a DIR
							else {
								String tempName = entry[i].getName().replace(' ', '_');
								if (tempName.lastIndexOf('.') != -1) type = tempName.substring(
										tempName.lastIndexOf('.')).toLowerCase();
							}
							String ahref = "<a onmousedown=\"dis()\" href=\"" + browser_name + "?sort="
									+ sortMode + "&amp;";
							String dlink = "&nbsp;"; // The "Download" link
							String elink = "&nbsp;"; // The "Edit" link
							String buf = Utils.conv2Html(entry[i].getName());
							if (!entry[i].canWrite()) buf = "<i>" + buf + "</i>";
							String link = buf; // The standard view link, uses Mime-type
							if (entry[i].isDirectory()) {
								if (entry[i].canRead() && Utils.USE_DIR_PREVIEW) {
									//Show the first DIR_PREVIEW_NUMBER directory entries in a tooltip
									File[] fs = entry[i].listFiles();
									if (fs == null) fs = new File[] {};
									Arrays.sort(fs, new FileComp());
									StringBuffer filenames = new StringBuffer();
									for (int i2 = 0; (i2 < fs.length) && (i2 < 10); i2++) {
										String fname = Utils.conv2Html(fs[i2].getName());
										if (fs[i2].isDirectory()) filenames.append("[" + fname + "];");
										else filenames.append(fname + ";");
									}
									if (fs.length > Utils.DIR_PREVIEW_NUMBER) filenames.append("...");
									else if (filenames.length() > 0) filenames
											.setLength(filenames.length() - 1);
									link = ahref + "dir=" + name + "\" title=\"" + filenames + "\">"
											+ FOL_IMG + "[" + buf + "]</a>";
								}
								else if (entry[i].canRead()) {
									link = ahref + "dir=" + name + "\">" + FOL_IMG + "[" + buf + "]</a>";
								}
								else link = FOL_IMG + "[" + buf + "]";
							}
							else if (entry[i].isFile()) { //Entry is file
								totalSize = totalSize + entry[i].length();
								fileCount = fileCount + 1;
								if (entry[i].canRead()) {
									dlink = ahref + "downfile=" + name + "\">Download</a>";
									//If you click at the filename
									if (Utils.USE_POPUP) link = ahref + "file=" + name + "\" target=\"_blank\">"
											+ buf + "</a>";
									else link = ahref + "file=" + name + "\">" + buf + "</a>";
									if (entry[i].canWrite()) { // The file can be edited
										//If it is a zip or jar File you can unpack it
										if (Utils.isPacked(name, true)) elink = ahref + "unpackfile=" + name
												+ "\">Unpack</a>";
										else elink = ahref + "editfile=" + name + "\">Edit</a>";
									}
									else { // If the file cannot be edited
										//If it is a zip or jar File you can unpack it
										if (Utils.isPacked(name, true)) elink = ahref + "unpackfile=" + name
												+ "\">Unpack</a>";
										else elink = ahref + "editfile=" + name + "\">View</a>";
									}
								}
								else {
									link = buf;
								}
							}
							out.println("<tr class=\"mouseout\" onmouseup=\"selrow(this, 2)\" "
									+ "onmouseover=\"selrow(this, 0);\" onmouseout=\"selrow(this, 1)\">");
							if (entry[i].canRead()) {
								out.println("<td align=center><input type=\"checkbox\" name=\"selfile\" value=\""+ name + "\" onmousedown=\"dis()\"></td>");
							}
							else {
								out.println("<td align=center><input type=\"checkbox\" name=\"selfile\" disabled></td>");
							}
							out.print("<td align=left> &nbsp;" + link +" "+dlink+"</td>");
									
						}
					
		
				}
			}%>
	</table>

	<p>
		<input type="hidden" name="dir" value="<%=request.getAttribute("dir")%>">
		<input type="hidden" name="sort" value="<%=sortMode%>">
		<input title="Delete all selected files and directories incl. subdirs" class="button" type="Submit" name="Submit" value="<%=Utils.DELETE_FILES%>"
		onclick="return confirm('Do you really want to delete the entries?')">
	</p>
	<p>
		<input title="Enter new dir or filename or the relative or absolute path" type="text" name="cr_dir">
		<input title="Create a new directory with the given name" class="button" type="Submit" name="Submit" value="<%=Utils.CREATE_DIR%>">
		<input title="Rename selected file or directory to the entered name" class="button" type="Submit" name="Submit" value="<%=Utils.RENAME_FILE%>">
	</p>
	</form>
	<form action="<%= browser_name%>" enctype="multipart/form-data" method="POST">
		<input type="hidden" name="dir" value="<%=request.getAttribute("dir")%>">
		<input type="hidden" name="sort" value="<%=sortMode%>">
		<input type="file" name="myFile">
		<input title="Upload selected file to the current working directory" type="Submit" class="button" name="Submit" value="Upload"
		onClick="javascript:popUp('<%= browser_name%>')">
	</form>
	<%
    }
		    
    %>
	<hr>
	
</body>
</html><%
    }
            
		
%>