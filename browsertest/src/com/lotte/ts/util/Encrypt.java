package com.lotte.ts.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class Encrypt {
	
	//SHA-256을 이용한 암호화 
	public static String getEncrypt(String input){
		String SHA = "";
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256");
			sh.update(input.getBytes());
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			SHA = sb.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			SHA = null;
		}
		System.out.println(SHA);
		return SHA;
	}
	
}
